package com.example.corsharing;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class userPage extends AppCompatActivity {
    private Button act_ExitUserPage, act_CurWeath, act_Search, act_map;
    private ArrayList<Auto> autos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_page);
        autos = getIntent().getParcelableArrayListExtra("autos");
    }

    public void addListenerOnButton(View v) {
        act_CurWeath = (Button) findViewById(R.id.btnCurWeath);
        act_Search = (Button) findViewById(R.id.btnSearchPage);
        act_ExitUserPage = (Button) findViewById(R.id.btnExitUserPage);
        act_map = findViewById(R.id.btnMapePage);
        act_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(".mapPage");
                startActivity(intent);
            }
        });
        act_ExitUserPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        act_CurWeath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(".curPage");
                startActivity(intent);
            }
        });

        act_Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(".searchAuto");
                intent.putParcelableArrayListExtra("autos", autos);
                startActivity(intent);
            }
        });

    }
}