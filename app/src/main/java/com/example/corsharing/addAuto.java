package com.example.corsharing;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class addAuto extends AppCompatActivity {

    private Button act_back_adminPage, act_add_auto;
    private ArrayList<Auto> autos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_auto);
        autos = getIntent().getParcelableArrayListExtra("autos");
    }

    public void addListenerOnButton(View v) {
        act_add_auto = (Button) findViewById(R.id.btnAddAuto);
        act_back_adminPage = (Button) findViewById(R.id.btnAddBack);
        act_add_auto.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View v) {
                                           EditText markAuto = (EditText)findViewById(R.id.editTextAddMarkAuto);
                                           EditText numberAuto = (EditText)findViewById(R.id.editTextAddNumberAuto);
                                           EditText priceAuto = (EditText)findViewById(R.id.textViewPrice);
                                           EditText levelComfort = (EditText)findViewById(R.id.editTextlevelComfort);
                                           int id = autos.get(-1).getId() + 1;
                                           String PriceAuto = priceAuto.getText().toString();
                                           String NumberAuto = numberAuto.getText().toString();
                                           String MarkAuto = markAuto.getText().toString();
                                           String LevelComfort = levelComfort.getText().toString();
                                           autos.add(new Auto(id, NumberAuto, MarkAuto, PriceAuto, LevelComfort));
                                           Intent intent = new Intent();
                                           intent.putExtra("autos", autos);
                                           setResult(RESULT_OK, intent);
                                           finish();
                                       }
                                   }
        );

        act_back_adminPage.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent intent = new Intent();
                                            intent.putExtra("autos", autos);
                                            setResult(RESULT_OK, intent);
                                            finish();
                                        }
                                    }
        );
    }
}