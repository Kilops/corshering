package com.example.corsharing;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class editAuto extends AppCompatActivity {
    private Button act_back, act_edit_auto;
    private ArrayList<Auto> autos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_auto);

        autos = getIntent().getParcelableArrayListExtra("autos");
        System.out.println(autos);
    }

    public void addListenerOnButton(View v) {
        act_edit_auto = (Button) findViewById(R.id.btnEditAuto);
        act_back = (Button) findViewById(R.id.btnEditBack);
        act_edit_auto.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                EditText id = (EditText)findViewById(R.id.editTextIdAuto);
                                                EditText priceAuto = (EditText)findViewById(R.id.editTextEditPriceAuto);
                                                int idAuto = Integer.parseInt(id.getText().toString());
                                                Auto auto;
                                                for(int i = 0; i < autos.size(); ++i){
                                                    Auto element = autos.get(i);
                                                    if (element.getId() == idAuto)
                                                    {
                                                        auto = element;
                                                        auto.setPrice(priceAuto.getText().toString());
                                                        autos.set(i, auto);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
        );

        act_back.setOnClickListener(new View.OnClickListener() {
                                                  @Override
                                                  public void onClick(View v) {
                                                      Intent intent = new Intent();
                                                      intent.putExtra("autos", autos);
                                                      setResult(RESULT_OK, intent);
                                                      finish();
                                                  }
                                              }
        );
    }
}