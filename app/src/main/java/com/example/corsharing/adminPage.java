package com.example.corsharing;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

public class adminPage extends AppCompatActivity {
    private Button act_add, act_edit, act_list_auto, act_exit;
    private ArrayList<Auto> autos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_page);
        autos = getIntent().getParcelableArrayListExtra("autos");
        System.out.println("test");
        System.out.println(autos);
    }


    public void addListenerOnButton(View v) {
        act_add = (Button) findViewById(R.id.btnAddAuto);
        act_edit = (Button) findViewById(R.id.btnEdit);
        act_list_auto = (Button) findViewById(R.id.btnForShowingAListOfCars);
        act_exit = (Button) findViewById(R.id.btnExit);

        act_add.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        Intent intent = new Intent(".addAuto");
                                                        intent.putParcelableArrayListExtra("autos", autos);
                                                        startActivityForResult(intent, 0);
                                                    }
                                                }
        );

        act_edit.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        Intent intent = new Intent(".editAuto");
                                                        intent.putParcelableArrayListExtra("autos", autos);
                                                        startActivityForResult(intent, 0);
                                                    }
                                                }
        );

        act_list_auto.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        Intent intent = new Intent(".listAuto");
                                                        intent.putParcelableArrayListExtra("autos", autos);
                                                        startActivity(intent);
                                                    }
                                                }
        );

        act_exit.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        finish();
                                                    }
                                                }
        );

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 0)
        {
            if(resultCode == RESULT_OK)
            {
                autos = data.getParcelableArrayListExtra("autos");;
            }
        }
    }
}