package com.example.corsharing;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class resultSearchPage extends AppCompatActivity {
    private ArrayList<Auto> autos;
    private String street;
    private int indexAuto;
    private Button act_back, act_next, act_order;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        autos = getIntent().getParcelableArrayListExtra("autos");
        street = getIntent().getExtras().getString("street");
        indexAuto = 0;
        setContentView(R.layout.activity_result_search_page);

        TextView levelComfort = findViewById(R.id.textNumberAuto3);
        TextView NumberAuto = findViewById(R.id.textNumberAuto);
        TextView PriceAuto = findViewById(R.id.textPriceAuto);

        levelComfort.setText(autos.get(indexAuto).getLevelComfort());
        NumberAuto.setText(autos.get(indexAuto).getNumber());
        PriceAuto.setText(autos.get(indexAuto).getPrice());
    }

    public void addListenerOnButton(View v) {
        act_back = (Button) findViewById(R.id.btnResultPageBack);
        act_next = (Button) findViewById(R.id.btnNext);
        act_order = (Button)findViewById(R.id.btnOrder);

        act_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        act_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        act_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ++indexAuto;
                if(indexAuto >= autos.size())
                {
                    indexAuto = 0;
                }
                TextView levelComfort = findViewById(R.id.textNumberAuto3);
                TextView NumberAuto = findViewById(R.id.textNumberAuto);
                TextView PriceAuto = findViewById(R.id.textPriceAuto);

                levelComfort.setText(autos.get(indexAuto).getLevelComfort());
                NumberAuto.setText(autos.get(indexAuto).getNumber());
                PriceAuto.setText(autos.get(indexAuto).getPrice());

            }
        });
    }
}