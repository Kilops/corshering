package com.example.corsharing;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    private Button act_change_adminPage, act_change_userPage;
    private ArrayList<Auto> autos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        autos = new ArrayList<Auto>();
        autos.add(new Auto(1, "1324", "aaaaaaa", "10", "business"));
        autos.add(new Auto(2, "1234", "aaaaaa", "9", "economy"));
        autos.add(new Auto(3, "1111", "aaaaa", "8", "comfort"));
        autos.add(new Auto(4, "2222", "aaaa", "7", "comfort"));
        autos.add(new Auto(5, "3333", "aaa", "6", "comfort"));
        autos.add(new Auto(6, "4444", "aa", "5", "comfort"));
        autos.add(new Auto(7, "5555", "a", "4", "comfort"));
        setContentView(R.layout.main_page);
    }

    public void addListenerOnButton(View v) {
        act_change_adminPage = (Button) findViewById(R.id.btnAdmin);
        act_change_userPage = (Button) findViewById(R.id.btnUser);
        act_change_adminPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(".adminPage");
                intent.putParcelableArrayListExtra("autos", autos);
                startActivityForResult(intent, 0);
            }
        }
        );

        act_change_userPage.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        Intent intent = new Intent(".userPage");
                                                        intent.putParcelableArrayListExtra("autos", autos);
                                                        startActivity(intent);
                                                    }
                                                }
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 0)
        {
            if(resultCode == RESULT_OK)
            {
                autos = data.getParcelableArrayListExtra("autos");;
            }
        }
    }
}