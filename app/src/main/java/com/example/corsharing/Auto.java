package com.example.corsharing;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Auto implements Parcelable {
    private int id;
    private String number;
    private String mark;
    private String price;
    private String levelComfort;

    public String getLevelComfort() {
        return levelComfort;
    }

    public void setLevelComfort(String levelComfort) {
        this.levelComfort = levelComfort;
    }

    public Auto(int id, String number, String mark, String price, String levelComfort) {
        this.id = id;
        this.number = number;
        this.mark = mark;
        this.price = price;
        this.levelComfort = levelComfort;
    }

    protected Auto(Parcel in) {
        id = in.readInt();
        number = in.readString();
        mark = in.readString();
        price = in.readString();
        levelComfort = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(number);
        dest.writeString(mark);
        dest.writeString(price);
        dest.writeString(levelComfort);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Auto> CREATOR = new Creator<Auto>() {
        @Override
        public Auto createFromParcel(Parcel in) {
            return new Auto(in);
        }

        @Override
        public Auto[] newArray(int size) {
            return new Auto[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }



    @Override
    public String toString() {
        return (("id: " + id + '\n' +
                "number: " + number + '\n' +
                "mark: " + mark + '\n' +
                "price: " + price + '\n' +
                "levelComfort: " + levelComfort));
    }
}

