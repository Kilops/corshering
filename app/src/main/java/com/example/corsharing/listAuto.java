package com.example.corsharing;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class listAuto extends AppCompatActivity {
    private ArrayList<Auto> autos;
    private Button act_exit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_auto);
        autos = getIntent().getParcelableArrayListExtra("autos");
        System.out.println(autos);
        ListView listView = (ListView) findViewById(R.id.listAuto);
        final ArrayAdapter<Auto> autoArrayAdapter;
        autoArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, autos);
        listView.setAdapter(autoArrayAdapter);
    }
    public void addListenerOnButton(View v) {
        act_exit = (Button) findViewById(R.id.btnListAutoExit);

        act_exit.setOnClickListener(new View.OnClickListener() {
                                                  @Override
                                                  public void onClick(View v) {
                                                      finish();
                                                  }
                                              }
        );
    }
}
