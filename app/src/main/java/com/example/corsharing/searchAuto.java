package com.example.corsharing;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import java.util.ArrayList;

public class searchAuto extends AppCompatActivity {
    private String levelComfort;
    private Button act_back, act_search;
    private ArrayList<Auto> autos;
    private Boolean flagCheckButtonLevelComfort = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_auto);
        autos = getIntent().getParcelableArrayListExtra("autos");
        RadioGroup radioGroup = findViewById(R.id.radioGroup);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioButtonComfort:
                        levelComfort = "comfort";
                        flagCheckButtonLevelComfort = true;
                        break;
                    case R.id.radioButtonBusiness:
                        levelComfort = "business";
                        flagCheckButtonLevelComfort = true;
                        break;
                    case R.id.radioButtonEconomy:
                        levelComfort = "economy";
                        flagCheckButtonLevelComfort = true;
                        break;
                    default:
                        break;
                }
            }
        }
        );
    }

    public void addListenerOnButton(View v) {
        act_back = (Button) findViewById(R.id.btnSearchBack);
        act_search = (Button) findViewById(R.id.btnSearch);

        act_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        act_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText street = (EditText) findViewById(R.id.editStreet);
                if (flagCheckButtonLevelComfort == true) {
                    ArrayList<Auto> newAutos = new ArrayList<Auto>();
                    for (int i = 0; i < autos.size(); ++i) {
                        if (autos.get(i).getLevelComfort().equals(levelComfort)) {
                            newAutos.add(autos.get(i));
                        }
                    }
                    if(newAutos.size() != 0) {
                        Intent intent = new Intent(".resultSearchPage");
                        intent.putParcelableArrayListExtra("autos", newAutos);
                        intent.putExtra("street", street.getText().toString());
                        startActivity(intent);
                    }
                }
            }
        });
    }
}